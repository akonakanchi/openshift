FROM ubuntu:16.04

RUN apt-get update && apt-get install -y python python.pip net-tools

RUN pip install flask

COPY main.py /opt/

ENTRYPOINT FLASK_APP=/opt/main.py flask run --host=0.0.0.0 --port=8090